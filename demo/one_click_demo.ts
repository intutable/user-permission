import path from "path"
import * as readline from 'readline'
import { existsSync, unlinkSync } from "fs"
import { Core, CoreRequest } from "@intutable/core"
import { addRole, addRoleToUser, deleteCondition, deleteRole, getCondition, getRoles, isPermitted, listConditions, listPermissions, listRestrictions, listRolesOfUser, moveRole, permit, removeAllRolesFromUser, removeRoleFromUser, restrict, rolePermits, roleRestricts, setCondition } from "../src/requests"

const DATABASE_PLUGIN_PATH = path.join(__dirname, "../node_modules/@intutable/database")
const USER_PERMISSION_PLUGIN_PATH = path.join(__dirname, "../node_modules/@intutable/user-permission")
const PLUGIN_PATH = path.join(__dirname, "../")

let rl: readline.Interface
let core: Core

const USERS: string[] = ["Tom", "Anja"]

async function main() {
    rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    if (existsSync(path.join(__dirname, "../data.db"))) {
        unlinkSync(path.join(__dirname, "../data.db"))
    }

    core = await Core.create([DATABASE_PLUGIN_PATH, USER_PERMISSION_PLUGIN_PATH, PLUGIN_PATH])

    await logStatus("Start", false, false, false, false)

    await testRoles()
    await testUsers()
    await testConditions()
    await testRules()

    console.log("End of demo")
    await core.plugins.closeAll()
    rl.close()
}

async function testRoles() {
    await core.events.request(addRole("student"))
    await core.events.request(addRole("teacher"))
    await core.events.request(addRole("admin"))
    await logStatus(`Roles "student", "teacher", "admin" added`, true, false, false, false)

    await core.events.request(addRole("some other role", 2))
    await logStatus(`Roles "some other role" added at index 2`, true, false, false, false)

    await core.events.request(moveRole("some other role", 0))
    await logStatus(`Roles "some other role" moved to index 0`, true, false, false, false)

    await core.events.request(deleteRole("some other role"))
    await core.events.request(deleteRole("teacher"))
    await logStatus(`Roles "some other role" and "teacher" deleted`, true, false, false, false)
}

async function testUsers() {
    await core.events.request(addRole("tutor"))
    await logStatus(`Role "tutor" added`, true, false, false, false)

    await core.events.request(addRoleToUser("Tom", "student"))
    await core.events.request(addRoleToUser("Tom", "tutor"))
    await logStatus(`Roles "student", "tutor" assigned to the user "Tom"`, false, true, false, false)

    await core.events.request(removeRoleFromUser("Tom", "tutor"))
    await logStatus(`Role "tutor" removed from the user "Tom"`, false, true, false, false)
}

async function testConditions() {
    await logStatus(`No condition set`, false, false, true, false)

    await core.events.request(setCondition("Has name as parameter", {
        type: "object",
        properties: {
            name: { type: "string"},
        },
        required: ["name"],
    }))
    await core.events.request(setCondition("Has id as parameter", {
        type: "object",
        properties: {
            id: { type: "number"},
        },
        required: ["id"],
    }))
    await core.events.request(setCondition("everything", {}))
    await logStatus(`Conditions "Has name as parameter", "Has id as parameter" and "everything" set`, false, false, true, false)

    await core.events.request(deleteCondition("Has name as parameter"))
    await logStatus(`Condition "Has name as parameter" deleted`, false, false, true, false)
}

async function testRules() {
    core.events.listenForRequests("c", "m", async (c: CoreRequest) => {})

    await logStatus(`No permission / restriction set`, false, false, false, true)
    
    await core.events.request(restrict("student", "Has id as parameter"))
    await logStatus(`Restrict "Has id as parameter" for role "student"`, false, false, false, true)

    await core.events.request(permit("admin", "everything"))
    await logStatus(`Permit "everything" for role "admin"`, false, false, false, true)

    await testRolePermits("student", { channel: "c", method: "m" })
    await testRoleRestricts("student", { channel: "c", method: "m" })

    await testRolePermits("admin", { channel: "c", method: "m" })
    await testRoleRestricts("admin", { channel: "c", method: "m" })

    await testRoleRestricts("student", { channel: "c", method: "m", id: 4 })
    await testRoleRestricts("admin", { channel: "c", method: "m", id: 4 })

    await logStatus("Reminder", true, true, false, true)

    await testIsPermitted("Tom", { channel: "c", method: "m" })
    await testIsPermitted("Tom", { channel: "c", method: "m", id: 4 })

    await core.events.request(addRoleToUser("Tom", "admin"))
    await logStatus(`Role "admin" assigned to the user "Tom"`, true, true, false, true)

    await testIsPermitted("Tom", { channel: "c", method: "m" })
    await testIsPermitted("Tom", { channel: "c", method: "m", id: 4 })

    await core.events.request(moveRole("admin", 0))
    await logStatus(`Role "admin" moved to index 0`, true, true, false, true)
        
    await testIsPermitted("Tom", { channel: "c", method: "m" })
    await testIsPermitted("Tom", { channel: "c", method: "m", id: 4 })
}

async function testRolePermits(role: string, coreRequests: CoreRequest) {
    const stringifiedCR = JSON.stringify(coreRequests, null, 2)
    console.log(`Call "rolePermits" for the role "${role}" and for the core-request ${stringifiedCR}`)
    console.log(`Result: ${await core.events.request(rolePermits(role, coreRequests))}`)
    console.log()
    await waitForEnter()
}

async function testRoleRestricts(role: string, coreRequests: CoreRequest) {
    const stringifiedCR = JSON.stringify(coreRequests, null, 2)
    console.log(`Call "roleRestricts" for the role "${role}" and for the core-request ${stringifiedCR}`)
    console.log(`Result: ${await core.events.request(roleRestricts(role, coreRequests))}`)
    console.log()
    await waitForEnter()
}

async function testIsPermitted(user: string, coreRequests: CoreRequest) {
    const stringifiedCR = JSON.stringify(coreRequests, null, 2)
    console.log(`Call "isPermitted" for the user "${user}" and for the core-request ${stringifiedCR}`)
    console.log(`Result: ${await core.events.request(isPermitted(user, coreRequests))}`)
    console.log()
    await waitForEnter()
}

async function logStatus(message: string, 
                        logRoles: boolean, logUsers: boolean, 
                        logConditions: boolean, logRules: boolean,
                        gapLines: number = 0) {

    console.log("+-" + "-".repeat(message.length) + "-+")
    console.log("| " + message + " |")
    console.log("+-" + "-".repeat(message.length) + "-+")

    const roles = await core.events.request(getRoles()) as string[]
    const conditions = await core.events.request(listConditions()) as string[]

    if (logRoles) {
        console.log(`Roles (low to high priority): ${listToString(roles)}`)
        console.log()
    }

    if (logUsers) {
        console.log(`Roles of users: ${roles.length == 0 ? "empty" : ""}`)
        for (const userId of USERS) {
            const rolesOfUser = await core.events.request(listRolesOfUser(userId)) as string[]
            if (rolesOfUser.length > 0) {
                console.log(`User ${userId} has the roles ${listToString(rolesOfUser)}`)
            }
        }
        console.log()
    }

    if (logConditions) {
        console.log(`Conditions: ${conditions.length == 0 ? "empty" : ""}`)
        for (const condition of await conditions) {
            const jsonSchema = JSON.stringify(await core.events.request(getCondition(condition)) as object, null, 2)
            console.log(`Condition "${condition}" has the json-schema ${jsonSchema}`)
        }
        console.log()
    }

    if (logRules) {
        console.log("Permissions / Restrictions:")
        for (const role of roles) {
            const permissions = await core.events.request(listPermissions(role)) as string[]
            const restrictions = await core.events.request(listRestrictions(role)) as string[]
            if (permissions.length > 0 && restrictions.length > 0) {
                console.log(`Role ${role} permits ${listToString(permissions)} and restricts ${listToString(restrictions)}`)
            } else if (permissions.length > 0 && restrictions.length == 0) {
                console.log(`Role ${role} permits ${listToString(permissions)}`)
            } else if (permissions.length == 0 && restrictions.length > 0) {
                console.log(`Role ${role} restricts ${listToString(restrictions)}`)
            }
        }
        console.log()
    } 

    await waitForEnter()
    if (gapLines > 0) {
        console.log("\n".repeat(gapLines - 1))
    }
}

function waitForEnter() {
    return new Promise(resolve => {
        rl.question('', (answer) => resolve(answer))
    })
}

function listToString(list: string[]): string {
    return "[ " + list.map(str => `"${str}"`).join(", ") + " ]"
}

main()

