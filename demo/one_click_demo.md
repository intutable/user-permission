# 1-Click-Demo

To execute the 1-Click-Demo, run the following commands in your shell
```
git clone --recurse-submodules https://gitlab.com/intutable/user-permission
cd user-permission
npx rimraf data.db
tsc ./demo/one_click_demo.ts --esModuleInterop
node ./demo/one_click_demo.js && npx rimraf ./demo/one_click_demo.js && npx rimraf ./src/requests.js
```

If you are using PowerShell on Windows, you might need to use these commands instead
```
git clone --recurse-submodules https://gitlab.com/intutable/user-permission
cd user-permission
cmd /c "npx rimraf data.db && tsc ./demo/one_click_demo.ts --esModuleInterop && node ./demo/one_click_demo.js && npx rimraf ./demo/one_click_demo.js && npx rimraf ./src/requests.js"
```