import { EventSystem } from "@intutable/core"
import { createTable, deleteRow, insert, listTables, select } from "@intutable/database/dist/requests"

import { 
    CONDITIONS_TABLE_CFG, ROLES_TABLE_CFG, RULES_TABLE_CFG, USER_ROLES_TABLE_CFG 
} from "./config"

import { 
    getConditionsFromTableRowContent, getRolesFromTableRowContent, getRulesFromTableRowContent, 
    getTableRowContentForConditions, getTableRowContentForRoles, getTableRowContentForRules, 
    getTableRowContentForUserRoles, getUserRolesFromTableRowContent 
} from "./convert"

import { 
    getConditions, getRoles, getRules, getUserRoles, init
} from "./permission"

import { TableConfig, TableRow, TableRowContent } from "./types"
import { getRowContent, includesTableRowContent } from "./utils"

let eventSystem: EventSystem

let tableRowsRoles: TableRow[] = []
let tableRowsConditions: TableRow[] = []
let tableRowsUserRoles: TableRow[] = []
let tableRowsRules: TableRow[] = []

export async function initDatabaseAccess(_eventSystem: EventSystem) {
    eventSystem = _eventSystem
    await initDatabase()
    initData()
}

export async function updateDatabase() {
    const roleNewContent    	= getTableRowContentForRoles(getRoles())
    const conditionsNewContent  = getTableRowContentForConditions(getConditions())
    const userRoleNewContent    = getTableRowContentForUserRoles(getUserRoles())
    const ruleNewContent        = getTableRowContentForRules(getRules())
    await updateRolesTable(roleNewContent)
    await updateConditionsTable(conditionsNewContent)
    await updateUserRolesTable(userRoleNewContent)
    await updateRulesTable(ruleNewContent)
}

async function initDatabase() {
    const existingTables = await eventSystem.request(listTables()) as string[]
    await createTableIfNecessary(ROLES_TABLE_CFG, existingTables)
    await createTableIfNecessary(CONDITIONS_TABLE_CFG, existingTables)
    await createTableIfNecessary(USER_ROLES_TABLE_CFG, existingTables)
    await createTableIfNecessary(RULES_TABLE_CFG, existingTables)
    tableRowsRoles      = await readRowsFromTable(ROLES_TABLE_CFG)
    tableRowsConditions = await readRowsFromTable(CONDITIONS_TABLE_CFG)
    tableRowsUserRoles  = await readRowsFromTable(USER_ROLES_TABLE_CFG)
    tableRowsRules      = await readRowsFromTable(RULES_TABLE_CFG)
}

function initData() {
    const roles: TableRowContent[]      = getRowContent(tableRowsRoles)
    const conditions: TableRowContent[] = getRowContent(tableRowsConditions)
    const userRoles: TableRowContent[]  = getRowContent(tableRowsUserRoles)
    const rules: TableRowContent[]      = getRowContent(tableRowsRules)
    init(
        getRolesFromTableRowContent(roles),
        getConditionsFromTableRowContent(conditions),
        getUserRolesFromTableRowContent(userRoles),
        getRulesFromTableRowContent(rules)
    )
}

async function createTableIfNecessary(tableConfig: TableConfig, existingTables: string[]) {
    if (!existingTables.includes(tableConfig.tableName)) {
        await eventSystem.request(createTable(
            tableConfig.tableName,
            Object.values(tableConfig.columns)))
    }
}

async function updateRolesTable(newRowContent: TableRowContent[]) {
    if (await updateTable(ROLES_TABLE_CFG, tableRowsRoles, newRowContent)) {
        tableRowsRoles = await readRowsFromTable(ROLES_TABLE_CFG)
    }
}

async function updateConditionsTable(newRowContent: TableRowContent[]) {
    if (await updateTable(CONDITIONS_TABLE_CFG, tableRowsConditions, newRowContent)) {
        tableRowsConditions = await readRowsFromTable(CONDITIONS_TABLE_CFG)
    }
}

async function updateUserRolesTable(newRowContent: TableRowContent[]) {
    if (await updateTable(USER_ROLES_TABLE_CFG, tableRowsUserRoles, newRowContent)) {
        tableRowsUserRoles = await readRowsFromTable(USER_ROLES_TABLE_CFG)
    }
}

async function updateRulesTable(newRowContent: TableRowContent[]) {
    if (await updateTable(RULES_TABLE_CFG, tableRowsRules, newRowContent)) {
        tableRowsRules = await readRowsFromTable(RULES_TABLE_CFG)
    }
}

async function updateTable(tableConfig: TableConfig, oldRows: TableRow[], newRowContent: TableRowContent[]): Promise<boolean> {
    const rowsRemoved   = await deleteRemovedRows(tableConfig, oldRows, newRowContent)
    const rowsAdded     = await insertNewRows(tableConfig, oldRows, newRowContent)
    return rowsRemoved || rowsAdded
}

async function deleteRemovedRows(tableConfig: TableConfig, oldRows: TableRow[], newRowContent: TableRowContent[]): Promise<boolean> {
    const rowIDsToRemove = 
        oldRows
            .filter(({ id, ...row }) => !includesTableRowContent(newRowContent, row))
            .map(({ id }) => id)

    if (rowIDsToRemove.length > 0) {
        await eventSystem.request(deleteRow(tableConfig.tableName, ["id", "in", rowIDsToRemove]))
        return true
    } else {
        return false
    }
}

async function insertNewRows(tableConfig: TableConfig, oldRows: TableRow[], newRowContent: TableRowContent[]): Promise<boolean> {
    const oldRowContent = getRowContent(oldRows)
    const rowContentToAdd = newRowContent.filter(row => !includesTableRowContent(oldRowContent, row))
    if (rowContentToAdd.length > 0) {
        await eventSystem.request(insert(tableConfig.tableName, rowContentToAdd))
        return true
    } else {
        return false
    }
}

async function readRowsFromTable(tableConfig: TableConfig): Promise<TableRow[]> {
    return (await eventSystem.request(select(tableConfig.tableName))) as TableRow[]
}