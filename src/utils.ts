import { TableRow, TableRowContent } from "./types"

export function getRowContent(rows: TableRow[]): TableRowContent[] {
    return rows.map(({ id, ...row }) => row)
}

export function tableRowContentEquals(object1: TableRowContent, object2: TableRowContent): boolean {
    const keys1 = Object.keys(object1)
    const keys2 = Object.keys(object2)
    if (keys1.length !== keys2.length) {
        return false
    }

    const entries1 = Object.entries(object1)
        .sort(([key1, v1], [key2, v2]) => key1.localeCompare(key2))
    const entries2 = Object.entries(object2)
        .sort(([key1, v1], [key2, v2]) => key1.localeCompare(key2))

    for (let i = 0; i < entries1.length; i++) {
        if (entries1[i][0] != entries2[i][0] || entries1[i][1] != entries2[i][1]) {
            return false
        }
    }
    return true
}

export function includesTableRowContent(list: TableRowContent[], searchElement: TableRowContent): boolean {
    for (const element of list) {
        if (tableRowContentEquals(searchElement, element)) {
            return true
        }
    }
    return false
}

export function tableRowContentListDifference(a: TableRowContent[], b: TableRowContent[]): TableRowContent[] {
    return a.filter(element => !includesTableRowContent(b, element))
}

export function tableRowContentListsEqual(a: TableRowContent[], b: TableRowContent[]): boolean {
    return tableRowContentListDifference(a, b).length == 0 && tableRowContentListDifference(b, a).length == 0
}