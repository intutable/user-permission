import { 
    PluginLoader, CoreRequest, CoreResponse, MiddlewareResponse, MiddlewareResponseType 
} from "@intutable/core"

import { initDatabaseAccess, updateDatabase } from "./database"
import { USER_ID_FIELD } from "./config"
import * as pm from "./permission"

export async function init(plugin: PluginLoader) {
    await initDatabaseAccess(plugin.events)

    plugin
        .listenForRequests("user-permission")
        .on("getRoles", getRolesEndpoint)
        .on("addRole", addRoleEndpoint)
        .on("moveRole", moveRoleEndpoint)
        .on("deleteRole", deleteRoleEndpoint)

        .on("setCondition", setConditionEndpoint)
        .on("deleteCondition", deleteConditionEndpoint)
        .on("getCondition", getConditionEndpoint)
        .on("listConditions", listConditionsEndpoint)

        .on("addRoleToUser", addRoleToUserEndpoint)
        .on("removeRoleFromUser", removeRoleFromUserEndpoint)
        .on("removeAllRolesFromUser", removeAllRolesFromUserEndpoint)
        .on("listRolesOfUser", listRolesOfUserEndpoint)

        .on("permit", permitEndpoint)
        .on("restrict", restrictEndpoint)
        .on("ignore", ignoreEndpoint)
        .on("listPermissions", listPermissionsEndpoint)
        .on("listRestrictions", listRestrictionsEndpoint)
        .on("rolePermits", rolePermitsEndpoint)
        .on("roleRestricts", roleRestrictsEndpoint)
        .on("isPermitted", isPermittedEndpoint)

    plugin.addMiddleware(permissionMiddleware)
}

export async function getRolesEndpoint({}: CoreRequest): Promise<CoreResponse> {
    return pm.getRoles()
}

export async function addRoleEndpoint({ role, index }: CoreRequest): Promise<CoreResponse> {
    pm.addRole(role, index)
    await updateDatabase()
    return `added role ${role}`
}

export async function moveRoleEndpoint({ role, newIndex }: CoreRequest): Promise<CoreResponse> {
    pm.moveRole(role, newIndex)
    await updateDatabase()
    return `the role ${role} was moved to the index ${newIndex}`
}

export async function deleteRoleEndpoint({ role }: CoreRequest): Promise<CoreResponse> {
    pm.deleteRole(role)
    await updateDatabase()
    return `deleted role ${role}`
}

export async function setConditionEndpoint({ name, jsonSchema }: CoreRequest): Promise<CoreResponse> {
    pm.setCondition(name, jsonSchema)
    await updateDatabase()
    return `the condition ${name} was set`
}

export async function deleteConditionEndpoint({ name }: CoreRequest): Promise<CoreResponse> {
    pm.deleteCondition(name)
    await updateDatabase()
    return `the condition ${name} was deleted`
}

export async function getConditionEndpoint({ name }: CoreRequest): Promise<CoreResponse> {
    return pm.getCondition(name)
}

export async function listConditionsEndpoint(): Promise<CoreResponse> {
    return pm.listConditions()
}

export async function addRoleToUserEndpoint({ username, role }: CoreRequest): Promise<CoreResponse> {
    pm.addRoleToUser(username, role)
    await updateDatabase()
    return `the user ${username} was assigned the role ${role}`
}

export async function removeRoleFromUserEndpoint({ username, role }: CoreRequest): Promise<CoreResponse> {
    pm.removeRoleFromUser(username, role)
    await updateDatabase()
    return `the user ${username} is no longer assigned the role ${role}`
}

export async function removeAllRolesFromUserEndpoint({ username }: CoreRequest): Promise<CoreResponse> {
    pm.removeAllRolesFromUser(username)
    await updateDatabase()
    return `all roles from the user ${username} were removed`
}

export async function listRolesOfUserEndpoint({ username }: CoreRequest): Promise<CoreResponse> {
    return pm.listRolesOfUser(username)
}

export async function permitEndpoint({ role, condition }: CoreRequest): Promise<CoreResponse> {
    pm.permit(role, condition)
    await updateDatabase()
    return `${condition} was permitted for the role ${role}`
}

export async function restrictEndpoint({ role, condition }: CoreRequest): Promise<CoreResponse> {
    pm.restrict(role, condition)
    await updateDatabase()
    return `${condition} was restricted for the role ${role}`
}

export async function ignoreEndpoint({ role, condition }: CoreRequest): Promise<CoreResponse> {
    pm.ignore(role, condition)
    await updateDatabase()
    return `${condition} will be ignored by the role ${role}`
}

export async function listPermissionsEndpoint({ role }: CoreRequest): Promise<CoreResponse> {
    return pm.listPermissions(role)
}

export async function listRestrictionsEndpoint({ role }: CoreRequest): Promise<CoreResponse> {
    return pm.listRestrictions(role)
}

export async function rolePermitsEndpoint({ role, request }: CoreRequest): Promise<CoreResponse> {
    return pm.rolePermits(role, request)
}

export async function roleRestrictsEndpoint({ role, request }: CoreRequest): Promise<CoreResponse> {
    return pm.roleRestricts(role, request)
}

export async function isPermittedEndpoint({ user, request }: CoreRequest): Promise<CoreResponse> {
    return pm.isPermitted(user, request)
}

async function permissionMiddleware(request: CoreRequest): Promise<MiddlewareResponse> {
    let requestPermitted: boolean = true
    if (request[USER_ID_FIELD]) {
        requestPermitted = pm.isPermitted(request[USER_ID_FIELD], request)
    }
    return {
        type: requestPermitted ? MiddlewareResponseType.Pass : MiddlewareResponseType.Reject, 
        payload: null
    }
}