import { CONDITIONS_TABLE_CFG, ROLES_TABLE_CFG, RULES_TABLE_CFG, USER_ROLES_TABLE_CFG } from "./config"
import {
    ConditionsType, PermissionType, RolesType, RulesType, TableRowContent, UserRolesType 
} from "./types"

interface RoleTableRowContent {
    role: string,
    index: number
}

interface ConditionTableRowContent {
    name: string,
    jsonSchema: string
}

interface UserRoleTableRowContent {
    user: string,
    role: string
}

interface RuleTableRowContent {
    role: string,
    condition: string,
    permissionType: PermissionType
}



// roles
export function getTableRowContentForRoles(roles: RolesType): TableRowContent[] {
    let rowContent: TableRowContent[] = []
    for (let index = 0; index < roles.length; index++) {
        rowContent.push({ 
            [ROLES_TABLE_CFG.columns.role.name]:    roles[index], 
            [ROLES_TABLE_CFG.columns.index.name]:   index 
        })
    }
    return rowContent
}

export function getRolesFromTableRowContent(rowContent: TableRowContent[]): RolesType {
    const roleRowContent = rowContent.map(row => ({ 
        role:   row[ROLES_TABLE_CFG.columns.role.name], 
        index:  row[ROLES_TABLE_CFG.columns.index.name]
    } as RoleTableRowContent))

    return roleRowContent
        .sort(({ index: i1 }, { index: i2 }) => i1 - i2)
        .map(({ role }) => role)
}



// conditions
export function getTableRowContentForConditions(conditions: ConditionsType): TableRowContent[] {
    let rowContent: TableRowContent[] = []
    for (const conditionName in conditions) {
        rowContent.push({ 
            [CONDITIONS_TABLE_CFG.columns.name.name]:       conditionName, 
            [CONDITIONS_TABLE_CFG.columns.jsonSchema.name]: JSON.stringify(conditions[conditionName]) 
        })
    }
    return rowContent
}

export function getConditionsFromTableRowContent(rowContent: TableRowContent[]): ConditionsType {
    const conditionsRowContent = rowContent.map(row => ({ 
        name:       row[CONDITIONS_TABLE_CFG.columns.name.name],
        jsonSchema: row[CONDITIONS_TABLE_CFG.columns.jsonSchema.name]
    } as ConditionTableRowContent))

    let conditions: ConditionsType = {}
    for (const { name, jsonSchema } of conditionsRowContent) {
        conditions[name] = JSON.parse(jsonSchema)
    }
    return conditions
}



// user roles
export function getTableRowContentForUserRoles(userRoles: UserRolesType): TableRowContent[] {
    let rowContent: TableRowContent[] = []
    for (const user in userRoles) {
        for (const role of userRoles[user]) {
            rowContent.push({ 
                [USER_ROLES_TABLE_CFG.columns.user.name]: user, 
                [USER_ROLES_TABLE_CFG.columns.role.name]: role 
            })
        }
    }
    return rowContent
}

export function getUserRolesFromTableRowContent(rowContent: TableRowContent[]): UserRolesType {
    const userRolesRowContent = rowContent.map(row => ({ 
        user: row[USER_ROLES_TABLE_CFG.columns.user.name],
        role: row[USER_ROLES_TABLE_CFG.columns.role.name]
    } as UserRoleTableRowContent))

    let userRoles: UserRolesType = {}
    for (const { user, role } of userRolesRowContent) {
        if (!userRoles[user]) userRoles[user] = []
        userRoles[user].push(role)
    }
    return userRoles
}



// rules
export function getTableRowContentForRules(rules: RulesType): TableRowContent[] {
    let rowContent: TableRowContent[] = []
    for (const role in rules) {
        for (const condition in rules[role]) {
            rowContent.push({ 
                [RULES_TABLE_CFG.columns.role.name]:            role, 
                [RULES_TABLE_CFG.columns.condition.name]:       condition, 
                [RULES_TABLE_CFG.columns.permissionType.name]:  rules[role][condition] 
            })
        }
    }
    return rowContent
}

export function getRulesFromTableRowContent(rowContent: TableRowContent[]): RulesType {
    const ruleRowContent = rowContent.map(row => ({ 
        role:           row[RULES_TABLE_CFG.columns.role.name],
        condition:      row[RULES_TABLE_CFG.columns.condition.name],
        permissionType: row[RULES_TABLE_CFG.columns.permissionType.name],
    } as RuleTableRowContent))

    let rules: RulesType = {}
    for (const { role, condition, permissionType } of ruleRowContent) {
        if (!rules[role]) rules[role] = {}
        rules[role][condition] = permissionType
    }
    return rules
}