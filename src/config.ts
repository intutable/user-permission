import { ColumnType } from "@intutable/database/dist/column"
import { TableConfig } from "./types"

export const USER_ID_FIELD = "userID"

export const ROLES_TABLE_CFG: TableConfig = {
    tableName: "roles", 
    columns: {
        id: { name: "id", type: ColumnType.increments },
        role: { name: "role", type: ColumnType.string },
        index: { name: "index", type: ColumnType.integer },
    }
}

export const CONDITIONS_TABLE_CFG: TableConfig = {
    tableName: "conditions",
    columns: {
        id: { name: "id", type: ColumnType.increments },
        name: { name: "name", type: ColumnType.string },
        jsonSchema: { name: "json_schema", type: ColumnType.string },
    }
}

export const USER_ROLES_TABLE_CFG: TableConfig = {
    tableName: "user_roles",
    columns: {
        id: { name: "id", type: ColumnType.increments },
        role: { name: "role", type: ColumnType.string },
        user: { name: "user", type: ColumnType.string },
    }
}

export const RULES_TABLE_CFG: TableConfig = {
    tableName: "rules",
    columns: {
        id: { name: "id", type: ColumnType.increments },
        role: { name: "role", type: ColumnType.string },
        condition: { name: "condition", type: ColumnType.string },
        permissionType: { name: "permission_type", type: ColumnType.integer },
    }
}
