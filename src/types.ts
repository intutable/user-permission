import { Column } from "@intutable/database/dist/column"

export type RolesType = string[]
export type ConditionsType = { [name: string]: object }
export type UserRolesType = { [user: string]: string[] }
export type RulesType = { [role: string]: { [condition: string]: PermissionType } }

export enum PermissionType {
    permitted = 0,
    restricted = 1
}

export interface TableRow {
    id: number,
    [index: string]: string | number | boolean
}

export type TableRowContent = { [index: string]: string | number | boolean }

export interface TableConfig {
    tableName: string,
    columns: { [index: string]: Column }
}