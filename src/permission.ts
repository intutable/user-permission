import Ajv from 'ajv'
import assert from 'assert'
import { CoreRequest } from '@intutable/core'
import { ConditionsType, PermissionType, RolesType, RulesType, UserRolesType } from './types'

let roles: RolesType = []
let conditions: ConditionsType = {}
let userRoles: UserRolesType = {}
let rules: RulesType = {}

export function reset() {
    roles = []
    conditions = {}
    userRoles = {}
    rules = {}
}

export function init(_roles: RolesType, _conditions: ConditionsType, _userRoles: UserRolesType, _rules: RulesType  ) {
    roles = _roles
    conditions = _conditions
    userRoles = _userRoles
    rules = _rules
}

// export function methods / endpoints

// role management

export function getRoles(): RolesType {
    return roles
}

export function getConditions(): ConditionsType {
    return conditions
}

export function getUserRoles(): UserRolesType {
    return userRoles
}

export function getRules(): RulesType {
    return rules
}

export function addRole(role: string, index: number) {
    assert(!roles.includes(role), `the role ${role} already exists`)
    if (0 <= index && index < roles.length) {
        roles.splice(index, 0, role)
    } else {
        roles.push(role)
    }
    rules[role] = {}
}

export function moveRole(role: string, newIndex: number) {
    assert(roles.includes(role), `the role ${role} does not exist`)
    assert(0 <= newIndex && newIndex < roles.length, `the index ${newIndex} is out of range`)
    
    const oldIndex = roles.indexOf(role)
    roles.splice(oldIndex, 1)
    roles.splice(newIndex, 0, role)
}

export function deleteRole(role: string) {
    assert(roles.includes(role), `the role ${role} does not exist`)
    const index = roles.indexOf(role)
    delete rules[role]
    for (const user in userRoles) {
        if (userRoles[user].includes(role)) {
            removeRoleFromUser(user, role)
        }
    }
    roles.splice(index, 1)
}


// condition management

export function setCondition(name: string, jsonSchema: object) {
    conditions[name] = jsonSchema
}

export function deleteCondition(name: string) {
    assert(conditions[name], `the condition ${name} does not exists`)
    delete conditions[name]
    for (const role in rules) {
        if (rules[role][name]) {
            delete rules[role][name]
        }
    }
}

export function getCondition(name: string): object {
    assert(conditions[name], `the condition ${name} does not exists`)
    return conditions[name]
}

export function listConditions(): string[] {
    return Object.keys(conditions)
}


// user-role management

export function addRoleToUser(username: string, role: string) {
    assert( roles.includes(role), `the role ${role} does not exist`)
    assert( !userRoles[username] || !userRoles[username].includes(role), 
            `the role ${role} does not exist`)
    if (!userRoles[username]) {
        userRoles[username] = []
    }
    userRoles[username].push(role)
}

export function removeRoleFromUser(username: string, role: string) {
    assert( userRoles[username] && userRoles[username].includes(role), 
            `the user ${username} is not assigned the role ${role}`)
    const indexOfRole = userRoles[username].indexOf(role)
    userRoles[username].splice(indexOfRole, 1)
}

export function removeAllRolesFromUser(username: string) {
    if (userRoles[username]) {
        delete userRoles[username]
    }
}

export function listRolesOfUser(username: string): string[] {
    if (userRoles[username]) {
        return userRoles[username]
    } else {
        return []
    }
}


// permissions and restrictions of roles

export function permit(role: string, condition: string) {
    assert(roles.includes(role), `the role ${role} does not exist`)
    assert(conditions[condition], `the condition ${condition} does not exist`)
    rules[role][condition] = PermissionType.permitted
}

export function restrict(role: string, condition: string) {
    assert(roles.includes(role), `the role ${role} does not exist`)
    assert(conditions[condition], `the condition ${condition} does not exist`)
    rules[role][condition] = PermissionType.restricted
}

export function ignore(role: string, condition: string) {
    assert(roles.includes(role), `the role ${role} does not exist`)
    assert(condition in conditions, `the condition ${condition} does not exist`)
    if (condition in rules[role]) {
        delete rules[role][condition]
    }
}

export function listPermissions(role: string): string[] {
    assert(roles.includes(role), `the role ${role} does not exist`)
    if (rules[role]) {
        return getConditionsWithPermissionType(role, PermissionType.permitted)
    } else {
        return []
    }
}

export function listRestrictions(role: string): string[] {
    assert(roles.includes(role), `the role ${role} does not exist`)
    if (rules[role]) {
        return getConditionsWithPermissionType(role, PermissionType.restricted)
    } else {
        return []
    }
}

export function rolePermits(role: string, request: CoreRequest): boolean {
    assert(roles.includes(role), `the role ${role} does not exist`)
    return rolePermitsRestricts(role, request, PermissionType.permitted)
}

export function roleRestricts(role: string, request: CoreRequest): boolean {
    assert(roles.includes(role), `the role ${role} does not exist`)
    return rolePermitsRestricts(role, request, PermissionType.restricted)
}

export function isPermitted(user: string, request: CoreRequest): boolean {
    if (!userRoles[user]) {
        return false
    }
    // permission types by roles ordered by role index (ascending)
    // bigger role index => higher priority
    const permissionTypesByRoles = userRoles[user]
        .sort((role1, role2) => roles.indexOf(role1) - roles.indexOf(role2))
        .map(role => getPermissionType(role, request))
        .filter(permissionType => permissionType != undefined)
    
    return  permissionTypesByRoles.length > 0 && 
            permissionTypesByRoles[permissionTypesByRoles.length - 1] == PermissionType.permitted
}


// private methods

function getConditionsWithPermissionType(role: string, permissionType: PermissionType): string[] {
    return Object.entries(rules[role])
        .filter(([condition, authType]) => authType == permissionType)
        .map(([condition, authType]) => condition)
}

function rolePermitsRestricts(role: string, request: CoreRequest, permissionType: PermissionType): boolean {
    const conditionNames = getConditionsWithPermissionType(role, permissionType)
    const ajv = new Ajv()
    for (const conditionName of conditionNames) {
        const validator = ajv.compile(conditions[conditionName])
        if (validator(request)) {
            return true
        }
    }
    return false
}

function getPermissionType(role: string, request: CoreRequest): PermissionType | undefined {
    if (rolePermitsRestricts(role, request, PermissionType.restricted)) 
        return PermissionType.restricted
    if (rolePermitsRestricts(role, request, PermissionType.permitted)) 
        return PermissionType.permitted
    return undefined
}