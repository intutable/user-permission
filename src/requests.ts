import { CoreRequest } from "@intutable/core"


// role management

/**
 * Get all existing roles
 */
export function getRoles(): CoreRequest {
    return {
        channel: "user-permission",
        method: "getRoles",
    }
}

/**
 * Add new role
 *
 * @param role - name of the new role
 * @param index - index of the new role in the list - bigger role index => higher priority
 */
export function addRole(role: string, index: number = -1): CoreRequest {
    return {
        channel: "user-permission",
        method: "addRole",
        role,
        index,
    }
}

/**
 * Move role to another index
 *
 * @param role - name of the role
 * @param newIndex - new index of the new role in the list - bigger role index => higher priority
 */
export function moveRole(role: string, newIndex: number): CoreRequest {
    return {
        channel: "user-permission",
        method: "moveRole",
        role,
        newIndex,
    }
}

/**
 * Delete an existing role
 *
 * @param role - name of the role
 */
export function deleteRole(role: string): CoreRequest {
    return {
        channel: "user-permission",
        method: "deleteRole",
        role,
    }
}


// condition management

/**
 * Set a json schema as a condition
 * 
 * @example ```
 * setCondition("conditionName", {
 *      type: "object",
 *      properties: {
 *          channel: { type: "string"},
 *          method: { type: "string"},
 *          str: { type: "string"},
 *          num: { type: "number"},
 *      },
 *      required: ["channel", "method", "str", "num"]
 * })
 * ```
 *
 * @param name - name of the condition
 * @param jsonSchema - the json schema the condition will be set to
 */
export function setCondition(name: string, jsonSchema: object) {
    return {
        channel: "user-permission",
        method: "setCondition",
        name,
        jsonSchema,
    }
}

/**
 * Delete an existing condition
 * 
 * @param name - the name of the condition
 */
export function deleteCondition(name: string) {
    return {
        channel: "user-permission",
        method: "deleteCondition",
        name,
    }
}

/**
 * Get the json schema of a condition
 * 
 * @param name - the name of the condition
 */
export function getCondition(name: string) {
    return {
        channel: "user-permission",
        method: "getCondition",
        name,
    }
}

/**
 * Get a list of all existing condition names
 */
export function listConditions() {
    return {
        channel: "user-permission",
        method: "listConditions",
    }
}


// user-role management

/**
 * Add a role to a user
 * 
 * @param username - the username
 * @param role - the role to be added
 */
export function addRoleToUser(username: string, role: string): CoreRequest {
    return {
        channel: "user-permission",
        method: "addRoleToUser",
        username,
        role,
    }
}

/**
 * Remove a role from a user
 * 
 * @param username - the username
 * @param role - the role to be removed
 */
export function removeRoleFromUser(username: string, role: string): CoreRequest {
    return {
        channel: "user-permission",
        method: "removeRoleFromUser",
        username,
        role,
    }
}

/**
 * Remove all roles of a user
 * 
 * @param username - the username
 */
export function removeAllRolesFromUser(username: string): CoreRequest {
    return {
        channel: "user-permission",
        method: "removeAllRolesFromUser",
        username,
    }
}

/**
 * List all roles of a user
 * 
 * @param username - the username
 */
export function listRolesOfUser(username: string): CoreRequest {
    return {
        channel: "user-permission",
        method: "listRolesOfUser",
        username,
    }
}


// permissions and restrictions of roles

/**
 * Permit a condition for a role
 * 
 * @param role - name of the role
 * @param condition - name of the condition
 */
export function permit(role: string, condition: string) {
    return {
        channel: "user-permission",
        method: "permit",
        role,
        condition,
    }
}

/**
 * Restrict a condition for a role
 * 
 * @param role - name of the role
 * @param condition - name of the condition
 */
export function restrict(role: string, condition: string) {
    return {
        channel: "user-permission",
        method: "restrict",
        role,
        condition,
    }
}

/**
 * Ignore a condition 
 * 
 * @param role - name of the role
 * @param condition - name of the condition
 */
export function ignore(role: string, condition: string) {
    return {
        channel: "user-permission",
        method: "ignore",
        role,
        condition,
    }
}

/**
 * List all permitted conditions of a role
 * 
 * @param role - name of the role
 */
export function listPermissions(role: string) {
    return {
        channel: "user-permission",
        method: "listPermissions",
        role,
    }
}

/**
 * List all restricted conditions of a role
 * 
 * @param role - name of the role
 */
export function listRestrictions(role: string) {
    return {
        channel: "user-permission",
        method: "listRestrictions",
        role,
    }
}

/**
 * Check if a role permits a core-request
 * 
 * @param role - name of the role
 * @param request - core-request
 */
export function rolePermits(role: string, request: CoreRequest) {
    return {
        channel: "user-permission",
        method: "rolePermits",
        role,
        request,
    }
}

/**
 * Check if a role restricts a core-request
 * 
 * @param role - name of the role
 * @param request - core-request
 */
export function roleRestricts(role: string, request: CoreRequest) {
    return {
        channel: "user-permission",
        method: "roleRestricts",
        role,
        request,
    }
}

/**
 * Check if a core-request is permitted for a user
 * 
 * @param user - name of the user
 * @param request - core-request
 */
export function isPermitted(user: string, request: CoreRequest) {
    return {
        channel: "user-permission",
        method: "isPermitted",
        user,
        request,
    }
}