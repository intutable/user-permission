# Overview

This plugin handles the permission-management for `xTable`

<br>

With this plugin you can
* define roles (like `admin`) and assign these roles to users
* permit or restrict core-requests for roles and hence for the users with the role

<br>

# Setup

To use the `user-permission` plugin within another plugin for `xTable`,
run the following command in the main-folder of your plugin:

```
npm install @intutable/user-permission
```

<br>

# Usage

You can communicate with the `user-permission` plugin via Core-Requests. For every supported request, you can import a function from the path `@intutable/user-permission/dist/requests` that builds the request for you. <br>
Since `user-permission` uses the `database` plugin, you have to pass the path to the database plugin to the Core together with the path to the `user-permission`. <br>

<br>

Code Example:

```typescript
import path from "path"
import { Core } from "@intutable/core"
import { getRoles } from "@intutable/user-permission/dist/requests"

const DATABASE_PLUGIN_PATH = path.join(__dirname, "../node_modules/@intutable/database")
const USER_PERMISSION_PLUGIN_PATH = path.join(__dirname, "../node_modules/@intutable/user-permission")

async function main() {
    const core = await Core.create([DATABASE_PLUGIN_PATH, USER_PERMISSION_PLUGIN_PATH])

    const request = getRoles()
    const response = (await core.events.request(request))
    console.log(response)
}

main()
```

## Manage Roles

A role is a `string`. The roles have a linear order. <br>
The managing of the roles is done through the requests
* `addRole`: creates a new role
* `moveRole`: moves a role to another position in the linear order
* `deleteRole`: deletes a role
* `getRoles`: returns a list of all existing roles sorted from lowest to highest priority

<br>

Code Example:
```typescript
console.log(await core.events.request(getRoles()))
// Output: []

await core.events.request(addRole("student"))
await core.events.request(addRole("teacher"))
await core.events.request(addRole("admin"))
console.log(await core.events.request(getRoles()))
// Output: ["student", "teacher", "admin"]

await core.events.request(addRole("some other role", 2))
console.log(await core.events.request(getRoles()))
// Output: ["student", "teacher", "some other role", "admin"]

await core.events.request(moveRole("some other role", 0))
console.log(await core.events.request(getRoles()))
// Output: ["some other role", "student", "teacher", "admin"]

await core.events.request(deleteRole("some other role"))
await core.events.request(deleteRole("teacher"))
console.log(await core.events.request(getRoles()))
// Output: ["student", "admin"]
```

## Manage Users

A user is a `string`. Each user is assigned a list of roles <br>
The managing of the roles is done through the requests
* `addRoleToUser`: adds a role to a user
* `removeRoleFromUser`: removes a role from a user
* `removeAllRolesFromUser`: removes all roles from a user
* `listRolesOfUser`: returns a list of all roles assigned to a user

<br>

Code Example:
```typescript
await core.events.request(addRole("student"))
await core.events.request(addRole("tutor analysis 1"))
await core.events.request(addRole("tutor algebra 1"))
await core.events.request(addRole("admin"))
console.log(await core.events.request(getRoles()))
// Output: ["student", "tutor analysis 1", "tutor algebra 1", "admin"]

await core.events.request(addRoleToUser("Tom", "student"))
await core.events.request(addRoleToUser("Tom", "tutor analysis 1"))
await core.events.request(addRoleToUser("Tom", "tutor algebra 1"))
console.log(await core.events.request(listRolesOfUser("Tom")))
// Output: ["student", "tutor analysis 1", "tutor algebra 1"]

await core.events.request(removeRoleFromUser("Tom", "tutor analysis 1"))
console.log(await core.events.request(listRolesOfUser("Tom")))
// Output: ["student", "tutor algebra 1"]

await core.events.request(removeAllRolesFromUser("Tom"))
console.log(await core.events.request(listRolesOfUser("Tom")))
// Output: []
```

## Manage Conditions

A (core-request-)condition is a JSON Schema with a unique name. A core-request fulfills a condition if and only if the core-request is a valid JSON object acording to the JSON Schema <br>
The managing of the conditions is done through the requests
* `setCondition`: sets a condition with a given name to be a given JSON schema
* `deleteCondition`: deletes a condition
* `getCondition`: returns the JSON schema of a condition
* `listConditions`: returns a list of all the names of the conditions

<br>

Code Example:
```typescript
await core.events.request(setCondition("Has name as parameter", {
    type: "object",
    properties: {
        name: { type: "string"},
    },
    required: ["name"],
}))
await core.events.request(setCondition("Has id as parameter", {
    type: "object",
    properties: {
        id: { type: "number"},
    },
    required: ["id"],
}))
console.log(await core.events.request(listConditions()))
// Output: ["Has name as parameter", "Has id as parameter"]

console.log(await core.events.request(getCondition("Has name as parameter")))
// Output: { type: "object", properties: { name: { type: "string"}, }, required: ["name"] }

await core.events.request(deleteCondition("Has name as parameter"))
console.log(await core.events.request(listConditions()))
// Output: ["Has id as parameter"]
```


## Manage permissions and restrictions

A role can permit / restrict (core-request-)conditions. <br>
A core-request is permitted by a role if the core-request fulfills a permitted condition <br>
A core-request is restricted by a role if the core-request fulfills a restricted condition <br>
A user can execute a core-request if and only if the user has a role r such that
* the role r permits the core-request,
* the role r does not restrict the core-request and
* the user has no rule with higher priority than r that restricts the core-request 

<br>

The managing of the permissions and restrictions is done through the requests
* `permit`: permits a condition for a role
* `restrict`: restricts a condition for a role
* `ignore`: remove permission / restriction status of a condition for a role meaning the condition is no longer permitted / restricted by the role
* `listPermissions`: returns a list of all the names of the permitted conditions of a role
* `listRestrictions`: returns a list of all the names of the restricted conditions of a role
* `rolePermits`: returns true if a given role permits a given core-request (otherwise false)
* `roleRestricts`: returns true if a given role restricts a given core-request (otherwise false)
* `isPermitted`: returns true if a given user is allowed to execute a given core-request (otherwise false)

<br>

Code Example:
```typescript
await core.events.request(addRole("name, but no number"))
await core.events.request(addRole("admin"))
await core.events.request(setCondition("has name as parameter", {
    type: "object",
    properties: { name: { type: "string"} },
    required: ["name"],
}))
await core.events.request(setCondition("has id as parameter", {
    type: "object",
    properties: { id: { type: "number"} },
    required: ["id"],
}))
await core.events.request(setCondition("no condition", {}))

await core.events.request(permit("name, but no number", "has name as parameter"))
await core.events.request(restrict("name, but no number", "has id as parameter"))
await core.events.request(permit("admin", "no condition"))

console.log(await core.events.request(listPermissions("name, but no number")))
// Output: ["has name as parameter"]
console.log(await core.events.request(listRestrictions("name, but no number")))
// Output: ["has id as parameter"]
console.log(await core.events.request(listPermissions("admin")))
// Output: ["no condition"]

console.log(await core.events.request(rolePermits("name, but no number", { 
    channel: "", method: "", 
})))
// Output: false
console.log(await core.events.request(rolePermits("name, but no number", { 
    channel: "", method: "", name: "x" 
})))
// Output: true

console.log(await core.events.request(roleRestricts("name, but no number", { 
    channel: "c", method: "m", 
})))
// Output: false
console.log(await core.events.request(roleRestricts("name, but no number", { 
    channel: "c", method: "m", id: 1,
})))
// Output: true

await core.events.request(addRoleToUser("Tom", "name, but no number"))

await core.events.request({ channel: "c", method: "m", userID: "Tom", name: "x" })
// no error

try { 
    await core.events.request({ channel: "c", method: "m", userID: "Tom", name: "x", id: 1 }) 
} catch { console.log("core-request denied") }
// Output: core-request denied

await core.events.request(addRoleToUser("Tom", "admin"))
await core.events.request({ channel: "c", method: "m", userID: "Tom", name: "x", id: 1 })
// no error
```

# Repository Structure

The source files are stored in the `src` folder, the test files are stored in the `tests` folder. <br>
The `src` folder contains the following files:
* `types.ts`: contains all type-definitions
* `config.ts`: contains all config-information (like table-names)
* `permission.ts`: implements the functionality of the endpoints and stores the data (roles, users, ...)
* `database.ts`: responsible for storing / loading the data (like roles, conditions, ...) into / from the database
* `convert.ts`: responsible for converting the raw data from `permission.ts` into table-format for `database.ts` and vice versa
* `index.ts`: contains all the endpoints
* `requests.ts`: contains request-builder-functions
* `utils.ts`: contains util-functions

<br>

Here is a sketch of what happens, when `user-permission` recieves a core-request. For this example we use the core-request `addRole`
```mermaid
sequenceDiagram
    participant Core
    participant index.ts
    participant database.ts
    participant convert.ts
    participant permission.ts
    Core ->> index.ts: addRoleEndpoint
    index.ts ->> permission.ts: addRole
    permission.ts -->> index.ts: void
    index.ts ->> database.ts: updateDatabase
    database.ts ->> permission.ts: get raw data
    permission.ts -->> database.ts: raw data
    database.ts ->> convert.ts: convert to table-format
    convert.ts -->> database.ts: data in table format
    database.ts ->> database.ts: update tables
    database.ts -->> index.ts: void
    index.ts -->> Core: CoreResponse
```


# Extending / Modifying user-permission

To add or change the functionality of the core-requests you must:
1. modify the associated functions of the core-requests in `permission.ts` and potentially add new ones
2. potentially modify or endpoint-functions in `index.ts`

If this change includes storing additional data or changing the data-format of existing data you must additionally

3. modify the conversion-functions in `convert.ts`
4. check if you need more tables or if you need more and / or different columns in the existing tables and update `config.ts` accordingly
5. if you need more tables, update the functions `initDatabase` and `updateDatabase` in `database.ts` so that the new tables are considered

# 1-Click-Demo

See our [1-Click Demo documentation](demo/one_click_demo.md) for details

# Accounting

The plugin was developed by Martin Koloseus

# License

The repository is Licensed under the Apache 2.0 License.