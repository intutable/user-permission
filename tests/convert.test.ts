import { 
    getTableRowContentForConditions, getTableRowContentForRoles, getTableRowContentForRules, 
    getTableRowContentForUserRoles 
} from '../src/convert'

import * as pm from '../src/permission'
import { PermissionType } from '../src/types'
import { jsonSchemas } from './test_utils'

describe("test table entry-generation", () => {
    beforeEach(() => {
        pm.reset()
    })

    test("table entries for roles", async () => {
        pm.addRole("r0", -1)
        pm.addRole("r1", -1)
        pm.addRole("r2", -1)
        pm.addRole("r3", -1)
        
        const entries = getTableRowContentForRoles(pm.getRoles())
        expect(entries).toEqual([
            { role: "r0", index: 0 },
            { role: "r1", index: 1 },
            { role: "r2", index: 2 },
            { role: "r3", index: 3 },
        ])
    })

    test("table entries for user roles", async () => {
        pm.addRole("r0", -1)
        pm.addRole("r1", -1)
        pm.addRoleToUser("u0", "r0")
        pm.addRoleToUser("u0", "r1")
        pm.addRoleToUser("u1", "r0")
        pm.addRoleToUser("u2", "r1")
        
        const entries = getTableRowContentForUserRoles(pm.getUserRoles())
        expect(entries).toEqual([
            { user: "u0", role: "r0" },
            { user: "u0", role: "r1" },
            { user: "u1", role: "r0" },
            { user: "u2", role: "r1" },
        ])
    })

    test("table entries for conditions", async () => {
        pm.setCondition("c0", jsonSchemas[0])
        pm.setCondition("c1", jsonSchemas[1])
        
        const entries = getTableRowContentForConditions(pm.getConditions())
        expect(entries).toEqual([
            { name: "c0", json_schema: JSON.stringify(jsonSchemas[0]) },
            { name: "c1", json_schema: JSON.stringify(jsonSchemas[1]) },
        ])
    })

    test("table entries for rules", async () => {
        pm.addRole("r0", -1)
        pm.addRole("r1", -1)
        pm.addRole("r2", -1)
        pm.setCondition("c0", jsonSchemas[0])
        pm.setCondition("c1", jsonSchemas[1])
        pm.permit("r0", "c0")
        pm.restrict("r0", "c1")
        pm.permit("r1", "c0")
        
        const entries = getTableRowContentForRules(pm.getRules())
        expect(entries).toEqual([
            { role: "r0", condition: "c0", permission_type: PermissionType.permitted },
            { role: "r0", condition: "c1", permission_type: PermissionType.restricted },
            { role: "r1", condition: "c0", permission_type: PermissionType.permitted },
        ])
    })
})