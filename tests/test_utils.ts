import { CoreRequest } from "@intutable/core"

import { 
    deleteConditionEndpoint, deleteRoleEndpoint, getRolesEndpoint, listConditionsEndpoint, 
    listRolesOfUserEndpoint, removeAllRolesFromUserEndpoint 
} from "../src"

import { 
    deleteCondition, deleteRole, getRoles, listRolesOfUser, removeAllRolesFromUser 
} from "../src/requests"

export let testRoles: string[] = []
export const ROLES_LENGTH = 10

export let testUsernames: string[] = []
export const USERNAMES_LENGTH = 15

export const testChannel = "c"
export const testMethod = "m"
export const jsonSchemas = [
    {
        type: "object",
        properties: {
            channel: { type: "string"},
            method: { type: "string"},
            str: { type: "string"},
            num: { type: "number"},
        },
        required: ["channel", "method", "str", "num"],
    },
    {
        type: "object",
        properties: {
            channel: { type: "string"},
            method: { type: "string"},
            foo: { type: "string"},
        },
        required: ["channel", "method", "foo"],
    },
]
export const coreRequests: CoreRequest[] = [
    { channel: testChannel, method: testMethod, str: "s", num: 5 },
    { channel: testChannel, method: testMethod, foo: "f" },
    { channel: testChannel, method: testMethod, bar: "b" },
]

export let testConditionNames: string[] = []
export const CONDITION_NAMES_LENGTH = 3

export async function generateTestData() {
    const roles = await getRolesEndpoint(getRoles()) as string[]
    for (let i = 0; testRoles.length < ROLES_LENGTH; i++) {
        if (!roles.includes(`role${i}`)) {
            testRoles.push(`role${i}`)
        }
    }

    for (let i = 0; testUsernames.length < USERNAMES_LENGTH; i++) {
        const roles = await listRolesOfUserEndpoint(listRolesOfUser(`user${i}`)) as string[]
        if (roles.length == 0) {
            testUsernames.push(`user${i}`)
        }
    }

    const conditions = await listConditionsEndpoint() as string[]
    for (let i = 0; testConditionNames.length < CONDITION_NAMES_LENGTH; i++) {
        if (!conditions.includes(`condition${i}`)) {
            testConditionNames.push(`condition${i}`)
        }
    }
}

export async function deleteTestDataFromTable() {
    const existingRoles = await getRolesEndpoint(getRoles()) as string[]
    for (const role of testRoles) {
        if (existingRoles.includes(role)) {
            await deleteRoleEndpoint(deleteRole(role))
        }
    }

    for (const username of testUsernames) {
        await removeAllRolesFromUserEndpoint(removeAllRolesFromUser(username))
    }

    const existingConditions = await listConditionsEndpoint() as string[]
    for (const conditionName of testConditionNames) {
        if (existingConditions.includes(conditionName)) {
            await deleteConditionEndpoint(deleteCondition(conditionName))
        }
    }
}