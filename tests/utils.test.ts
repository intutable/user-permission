import { 
    tableRowContentEquals, tableRowContentListDifference, tableRowContentListsEqual 
} from "../src/utils"

const testObject0 = {
    i0: "s0",
    i1: 1,
    i2: true,
}

const testObject1 = {
    i0: "s0",
    i1: 1,
    i2: true,
}

const testObjectX = {
    i0: "s0",
    i1: 1,
    x: "x"
}

describe("utils test", () => {
    test("table-row-content equals", () => {
        expect(tableRowContentEquals(testObject0, testObject1)).toBeTruthy()
        expect(tableRowContentEquals(testObject0, testObjectX)).toBeFalsy()
        expect(tableRowContentEquals(testObject1, testObjectX)).toBeFalsy()
    })

    test("table-row-content list difference", () => {
        expect(tableRowContentListDifference([testObject0, testObjectX], [testObject1])).toEqual([testObjectX])
        expect(tableRowContentListDifference([testObject1], [testObject0, testObjectX])).toEqual([])
    })

    test("table-row-content lists equal", () => {
        expect(tableRowContentListsEqual([testObject0], [testObject1])).toBeTruthy()
        expect(tableRowContentListsEqual([testObject0, testObjectX], [testObjectX, testObject1])).toBeTruthy()
        expect(tableRowContentListsEqual([testObject0, testObjectX], [testObject1])).toBeFalsy()
    })
})
