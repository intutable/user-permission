import path from "path"
import { Core } from "@intutable/core"

import { 
    addRole, addRoleToUser, deleteRole, listRolesOfUser, getRoles, removeRoleFromUser, 
    removeAllRolesFromUser, moveRole, isPermitted, setCondition, deleteCondition, 
    getCondition, listConditions, permit, restrict, ignore, listPermissions, listRestrictions, 
    rolePermits, roleRestricts 
} from "../src/requests"

import { 
    coreRequests, deleteTestDataFromTable, generateTestData, jsonSchemas, 
    testChannel, testConditionNames, testMethod, testRoles, testUsernames 
} from "./test_utils"

const DATABASE_PLUGIN_PATH = path.join(__dirname, "../node_modules/@intutable/database")
const PLUGIN_PATH = path.join(__dirname, "../")

let core: Core

beforeAll(async () => {
    core = await Core.create([DATABASE_PLUGIN_PATH, PLUGIN_PATH])
    generateTestData()
})

afterAll(async () => {
    await deleteTestDataFromTable()
    await core.plugins.closeAll()
})

describe("test role management", () => {
    beforeEach(async () => {
        const existingRoles = await core.events.request(getRoles()) as string[]
        for (const role of testRoles) {
            if (existingRoles.includes(role)) {
                await core.events.request(deleteRole(role))
            }
        }
    })
    
    test("add role, no index given", async () => {
        await core.events.request(addRole(testRoles[0]))
        await core.events.request(addRole(testRoles[1]))
        
        const roles = await core.events.request(getRoles()) as string[]
        expect(roles).toContain(testRoles[0])
        expect(roles).toContain(testRoles[1])
    })

    test("add role index given", async () => {
        await core.events.request(addRole(testRoles[0], 0))
        await core.events.request(addRole(testRoles[1], 0))
        await core.events.request(addRole(testRoles[2], 2))
        await core.events.request(addRole(testRoles[3], 1))
        
        const roles = await core.events.request(getRoles()) as string[]

        expect(roles.length).toBeGreaterThanOrEqual(4)
        expect(roles.slice(0, 4)).toEqual([testRoles[1], testRoles[3], testRoles[0], testRoles[2]])
    })

    test("move role", async () => {
        await core.events.request(addRole(testRoles[0], 0))
        await core.events.request(addRole(testRoles[1], 1))
        await core.events.request(addRole(testRoles[2], 2))

        await core.events.request(moveRole(testRoles[2], 0))
        await core.events.request(moveRole(testRoles[0], 2))

        const roles = await core.events.request(getRoles()) as string[]

        expect(roles.length).toBeGreaterThanOrEqual(3)
        expect(roles.slice(0, 3)).toEqual([testRoles[2], testRoles[1], testRoles[0]])
    })

    test("delete existing role", async () => {
        await core.events.request(addRole(testRoles[0]))

        const rolesBeforeDeletion = await core.events.request(getRoles()) as string[]
        expect(rolesBeforeDeletion).toContain(testRoles[0])
        const rolesWithoutUser0: string[] = rolesBeforeDeletion.filter(role => role != testRoles[0])
        
        await core.events.request(deleteRole(testRoles[0]))
        
        const rolesAfterDeletion = await core.events.request(getRoles()) as string[]

        expect(rolesAfterDeletion).not.toContain(testRoles[0])
        expect(rolesAfterDeletion).toEqual(rolesWithoutUser0)
    })

})

describe("test condition management", () => {
    beforeEach(async () => {
        const existingConditions = await core.events.request(listConditions()) as string[]
        for (const conditionName of testConditionNames) {
            if (existingConditions.includes(conditionName)) {
                await core.events.request(deleteCondition(conditionName))
            }
        }
    })

    test("set condition (new conditions)", async () => {
        await core.events.request(setCondition(testConditionNames[0], jsonSchemas[0]))
        await core.events.request(setCondition(testConditionNames[1], jsonSchemas[1]))
        
        const conditions = await core.events.request(listConditions()) as string[]
        expect(conditions).toContain(testConditionNames[0])
        expect(conditions).toContain(testConditionNames[1])

        const condition0 = await core.events.request(getCondition(testConditionNames[0])) as object
        const condition1 = await core.events.request(getCondition(testConditionNames[1])) as object
        expect(condition0).toEqual(jsonSchemas[0])
        expect(condition1).toEqual(jsonSchemas[1])
    })

    test("set condition (override condition)", async () => {
        await core.events.request(setCondition(testConditionNames[0], jsonSchemas[0]))
        await core.events.request(setCondition(testConditionNames[0], jsonSchemas[1]))

        const condition0 = await core.events.request(getCondition(testConditionNames[0])) as object
        expect(condition0).toEqual(jsonSchemas[1])
    })

    test("delete condition", async () => {
        await core.events.request(setCondition(testConditionNames[0], jsonSchemas[0]))
        await core.events.request(setCondition(testConditionNames[1], jsonSchemas[1]))

        await core.events.request(deleteCondition(testConditionNames[0]))
        await core.events.request(deleteCondition(testConditionNames[1]))
        
        const conditions = await core.events.request(listConditions()) as string[]
        expect(conditions).not.toContain(testConditionNames[0])
        expect(conditions).not.toContain(testConditionNames[1])
    })

})

describe("test user-role management", () => {
    beforeAll(async () => {
        const existingRoles = await core.events.request(getRoles())
        for (const role of testRoles) {
            if (!existingRoles.includes(role)) {
                await core.events.request(addRole(role))
            }
        }
    })

    test("add roles to users", async () => {
        await core.events.request(addRoleToUser(testUsernames[0], testRoles[0]))
        await core.events.request(addRoleToUser(testUsernames[0], testRoles[1]))
        await core.events.request(addRoleToUser(testUsernames[1], testRoles[2]))

        const roleUser0 = await core.events.request(listRolesOfUser(testUsernames[0])) as string[]
        const roleUser1 = await core.events.request(listRolesOfUser(testUsernames[1])) as string[]
        
        expect(roleUser0).toEqual([testRoles[0], testRoles[1]])
        expect(roleUser1).toEqual([testRoles[2]])
    })

    test("remove roles from users", async () => {
        await core.events.request(addRoleToUser(testUsernames[4], testRoles[0]))
        await core.events.request(addRoleToUser(testUsernames[5], testRoles[1]))

        await core.events.request(removeRoleFromUser(testUsernames[4], testRoles[0]))
        await core.events.request(removeRoleFromUser(testUsernames[5], testRoles[1]))

        const rolesUser4 = await core.events.request(listRolesOfUser(testUsernames[4])) as string[]
        const rolesUser5 = await core.events.request(listRolesOfUser(testUsernames[5])) as string[]
        
        expect(rolesUser4).not.toContain(testRoles[0])
        expect(rolesUser5).not.toContain(testRoles[1])
    })

    test("remove all roles from users", async () => {
        await core.events.request(addRoleToUser(testUsernames[6], testRoles[0]))
        await core.events.request(addRoleToUser(testUsernames[6], testRoles[1]))
        await core.events.request(addRoleToUser(testUsernames[6], testRoles[2]))

        await core.events.request(removeAllRolesFromUser(testUsernames[6])) as string[]

        const roles = await core.events.request(listRolesOfUser(testUsernames[6])) as string[]
        
        expect(roles).toEqual([])
    })

})

describe("test permissions and restrictions of roles", () => {
    beforeAll(async () => {
        const existingRoles = await core.events.request(getRoles())
        for (const role of testRoles) {
            if (!existingRoles.includes(role)) {
                await core.events.request(addRole(role))
            }
        }
        for (const username of testUsernames) {
            await core.events.request(removeAllRolesFromUser(username))
        }
        await core.events.request(setCondition(testConditionNames[0], jsonSchemas[0]))
        await core.events.request(setCondition(testConditionNames[1], jsonSchemas[1]))
    })

    test("permit", async () => {
        await core.events.request(permit(testRoles[0], testConditionNames[0]))
        await core.events.request(permit(testRoles[0], testConditionNames[1]))

        const permissions = await core.events.request(listPermissions(testRoles[0]))

        expect(permissions).toEqual([testConditionNames[0], testConditionNames[1]])
    })

    test("restrict", async () => {
        await core.events.request(restrict(testRoles[1], testConditionNames[0]))
        await core.events.request(restrict(testRoles[1], testConditionNames[1]))

        const restrictions = await core.events.request(listRestrictions(testRoles[1]))

        expect(restrictions).toEqual([testConditionNames[0], testConditionNames[1]])
    })

    test("ignore", async () => {
        await core.events.request(permit(testRoles[0], testConditionNames[0]))
        await core.events.request(restrict(testRoles[0], testConditionNames[1]))

        await core.events.request(ignore(testRoles[0], testConditionNames[0]))
        await core.events.request(ignore(testRoles[0], testConditionNames[1]))

        const permissions = await core.events.request(listPermissions(testRoles[0]))
        const restrictions = await core.events.request(listRestrictions(testRoles[0]))

        expect(permissions).not.toContain(testConditionNames[0])
        expect(restrictions).not.toContain(testConditionNames[1])
    })

    test("role permits", async () => {
        const res00 = await core.events.request(rolePermits(testRoles[2], coreRequests[0])) as boolean
        const res01 = await core.events.request(rolePermits(testRoles[2], coreRequests[1])) as boolean
        const res02 = await core.events.request(rolePermits(testRoles[2], coreRequests[2])) as boolean
        expect(res00).toBeFalsy()
        expect(res01).toBeFalsy()
        expect(res02).toBeFalsy()

        await core.events.request(permit(testRoles[2], testConditionNames[0]))
        const res10 = await core.events.request(rolePermits(testRoles[2], coreRequests[0])) as boolean
        const res11 = await core.events.request(rolePermits(testRoles[2], coreRequests[1])) as boolean
        const res12 = await core.events.request(rolePermits(testRoles[2], coreRequests[2])) as boolean
        expect(res10).toBeTruthy()
        expect(res11).toBeFalsy()
        expect(res12).toBeFalsy()

        await core.events.request(permit(testRoles[2], testConditionNames[1]))
        const res20 = await core.events.request(rolePermits(testRoles[2], coreRequests[0])) as boolean
        const res21 = await core.events.request(rolePermits(testRoles[2], coreRequests[1])) as boolean
        const res22 = await core.events.request(rolePermits(testRoles[2], coreRequests[2])) as boolean
        expect(res20).toBeTruthy()
        expect(res21).toBeTruthy()
        expect(res22).toBeFalsy()
    })

    test("role restricts", async () => {
        const res00 = await core.events.request(roleRestricts(testRoles[3], coreRequests[0])) as boolean
        const res01 = await core.events.request(roleRestricts(testRoles[3], coreRequests[1])) as boolean
        const res02 = await core.events.request(roleRestricts(testRoles[3], coreRequests[2])) as boolean
        expect(res00).toBeFalsy()
        expect(res01).toBeFalsy()
        expect(res02).toBeFalsy()

        await core.events.request(restrict(testRoles[3], testConditionNames[0]))
        const res10 = await core.events.request(roleRestricts(testRoles[3], coreRequests[0])) as boolean
        const res11 = await core.events.request(roleRestricts(testRoles[3], coreRequests[1])) as boolean
        const res12 = await core.events.request(roleRestricts(testRoles[3], coreRequests[2])) as boolean
        expect(res10).toBeTruthy()
        expect(res11).toBeFalsy()
        expect(res12).toBeFalsy()

        await core.events.request(restrict(testRoles[3], testConditionNames[1]))
        const res20 = await core.events.request(roleRestricts(testRoles[3], coreRequests[0])) as boolean
        const res21 = await core.events.request(roleRestricts(testRoles[3], coreRequests[1])) as boolean
        const res22 = await core.events.request(roleRestricts(testRoles[3], coreRequests[2])) as boolean
        expect(res20).toBeTruthy()
        expect(res21).toBeTruthy()
        expect(res22).toBeFalsy()
    })

    test("isPermitted, no role / one role", async () => {
        await core.events.request(addRoleToUser(testUsernames[1], testRoles[4]))
        await core.events.request(addRoleToUser(testUsernames[2], testRoles[5]))

        await core.events.request(permit(testRoles[4], testConditionNames[0]))
        await core.events.request(restrict(testRoles[5], testConditionNames[0]))

        const result0 = await core.events.request(isPermitted(testUsernames[0], coreRequests[0])) as boolean
        const result1 = await core.events.request(isPermitted(testUsernames[1], coreRequests[0])) as boolean
        const result2 = await core.events.request(isPermitted(testUsernames[2], coreRequests[0])) as boolean
        
        expect(result0).toBeFalsy()
        expect(result1).toBeTruthy()
        expect(result2).toBeFalsy()
    })

    test("isPermitted, multiple roles", async () => {
        await core.events.request(addRoleToUser(testUsernames[3], testRoles[4]))
        await core.events.request(addRoleToUser(testUsernames[3], testRoles[5]))
        await core.events.request(addRoleToUser(testUsernames[3], testRoles[6]))

        await core.events.request(permit(testRoles[4], testConditionNames[0]))
        await core.events.request(restrict(testRoles[5], testConditionNames[0]))
        await core.events.request(ignore(testRoles[6], testConditionNames[0]))

        const result0 = await core.events.request(isPermitted(testUsernames[3], coreRequests[0])) as boolean
        expect(result0).toBeFalsy()

        await core.events.request(permit(testRoles[6], testConditionNames[0]))

        const result1 = await core.events.request(isPermitted(testUsernames[3], coreRequests[0])) as boolean
        expect(result1).toBeTruthy()
    })

})

describe("test middleware", () => {
    beforeAll(async () => {
        const existingRoles = await core.events.request(getRoles())
        for (const role of testRoles) {
            if (!existingRoles.includes(role)) {
                await core.events.request(addRole(role))
            }
        }
        for (const username of testUsernames) {
            await core.events.request(removeAllRolesFromUser(username))
        }
        await core.events.request(setCondition(testConditionNames[0], jsonSchemas[0]))
        await core.events.request(setCondition(testConditionNames[1], jsonSchemas[1]))

        await core.events.request(addRoleToUser(testUsernames[0], testRoles[0]))

        core.events.listenForRequests(testChannel, testMethod, async () => { return {} })
    })

    test("pass permitted requests", async () => {
        await core.events.request(permit(testRoles[0], testConditionNames[0]))
        await core.events.request(permit(testRoles[0], testConditionNames[1]))

        let requestsPassed: boolean
        try {
            await core.events.request({ ...coreRequests[0], userID: testUsernames[0] })
            await core.events.request({ ...coreRequests[1], userID: testUsernames[0] })
            requestsPassed = true
        } catch (error) {
            requestsPassed = false
        }

        expect(requestsPassed).toBeTruthy()
    })

    test("reject forbidden requests", async () => {
        await core.events.request(restrict(testRoles[0], testConditionNames[0]))
        await core.events.request(restrict(testRoles[0], testConditionNames[1]))
        
        let requestPassed: boolean
        try {
            await core.events.request({ ...coreRequests[0], userID: testUsernames[0] })
            requestPassed = true
        } catch (error) {
            requestPassed = false
        }
        expect(requestPassed).toBeFalsy()

        try {
            await core.events.request({ ...coreRequests[1], userID: testUsernames[0] })
            requestPassed = true
        } catch (error) {
            requestPassed = false
        }
        expect(requestPassed).toBeFalsy()
    })

})

