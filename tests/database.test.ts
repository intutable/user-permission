import path from "path"
import { Core } from "@intutable/core"
import { deleteRow, insert, listTables, select } from "@intutable/database/dist/requests"
import { initDatabaseAccess } from "../src/database"

import { 
    addRoleEndpoint, addRoleToUserEndpoint, getConditionEndpoint, getRolesEndpoint, 
    listConditionsEndpoint, listPermissionsEndpoint, listRestrictionsEndpoint, 
    listRolesOfUserEndpoint, permitEndpoint, restrictEndpoint, setConditionEndpoint 
} from "../src/index"

import { 
    addRole, addRoleToUser, getCondition, getRoles, listPermissions, listRestrictions, 
    listRolesOfUser, permit, restrict, setCondition } 
from "../src/requests"

import { CONDITIONS_TABLE_CFG, ROLES_TABLE_CFG, RULES_TABLE_CFG, USER_ROLES_TABLE_CFG } from "../src/config"
import { tableRowContentListDifference } from "../src/utils"

import { 
    deleteTestDataFromTable, generateTestData, jsonSchemas, testConditionNames, 
    testRoles, testUsernames } 
from "./test_utils"

import { 
    getTableRowContentForConditions, getTableRowContentForRoles, getTableRowContentForRules, 
    getTableRowContentForUserRoles 
} from "../src/convert"

import * as pm from "../src/permission"
import { PermissionType, TableConfig, TableRow, TableRowContent } from "../src/types"

const DATABASE_PLUGIN_PATH = path.join(__dirname, "../node_modules/@intutable/database")

let core: Core

beforeAll(async () => {
    core = await Core.create([DATABASE_PLUGIN_PATH])
    generateTestData()
})

afterAll(async () => {
    await deleteTestDataFromTable()
    await core.plugins.closeAll()
})

async function expectTableOK(tableConfig: TableConfig, expecedEntries: TableRowContent[]) {
    const rows = (await core.events.request(select(tableConfig.tableName))) as TableRow[]
    const rowsNoID = rows.map(({ id, ...row }) => row)
    expect(tableRowContentListDifference(rowsNoID, expecedEntries)).toEqual([])
    expect(tableRowContentListDifference(expecedEntries, rowsNoID)).toEqual([])
}

describe("test saving", () => {
    beforeEach(async () => {
        const existingTables = await core.events.request(listTables()) as string[]
        for (const tableConfig of [ ROLES_TABLE_CFG, CONDITIONS_TABLE_CFG, 
                                    USER_ROLES_TABLE_CFG, RULES_TABLE_CFG]) {
            if (existingTables.includes(tableConfig.tableName)) {
                await core.events.request(deleteRow(
                    tableConfig.tableName, []))
            }
        }
    })

    test("init creates empty tables", async () => {
        await initDatabaseAccess(core.events)

        const existingTables = await core.events.request(listTables()) as string[]
        for (const tableConfig of [ ROLES_TABLE_CFG, CONDITIONS_TABLE_CFG, 
                                    USER_ROLES_TABLE_CFG, RULES_TABLE_CFG]) {
            expect(existingTables).toContain(tableConfig.tableName)
        }
    })

    test("save roles", async () => {
        await initDatabaseAccess(core.events)
        await addRoleEndpoint(addRole(testRoles[0], -1))
        await addRoleEndpoint(addRole(testRoles[1], -1))

        await expectTableOK(ROLES_TABLE_CFG, getTableRowContentForRoles(pm.getRoles()))
    })

    test("save conditions", async () => {
        await initDatabaseAccess(core.events)
        await setConditionEndpoint(setCondition(testConditionNames[0], jsonSchemas[0]))
        await setConditionEndpoint(setCondition(testConditionNames[1], jsonSchemas[1]))

        await expectTableOK(CONDITIONS_TABLE_CFG, getTableRowContentForConditions(pm.getConditions()))
    })

    test("save user roles", async () => {
        await initDatabaseAccess(core.events)
        await addRoleEndpoint(addRole(testRoles[0], -1))
        await addRoleEndpoint(addRole(testRoles[1], -1))
        await addRoleToUserEndpoint(addRoleToUser(testUsernames[0], testRoles[0]))
        await addRoleToUserEndpoint(addRoleToUser(testUsernames[0], testRoles[1]))
        await addRoleToUserEndpoint(addRoleToUser(testUsernames[1], testRoles[0]))
        await addRoleToUserEndpoint(addRoleToUser(testUsernames[2], testRoles[1]))

        await expectTableOK(USER_ROLES_TABLE_CFG, getTableRowContentForUserRoles(pm.getUserRoles()))
    })

    test("save rules", async () => {
        await initDatabaseAccess(core.events)
        await addRoleEndpoint(addRole(testRoles[0], -1))
        await addRoleEndpoint(addRole(testRoles[1], -1))
        await addRoleEndpoint(addRole(testRoles[2], -1))
        await setConditionEndpoint(setCondition(testConditionNames[0], jsonSchemas[0]))
        await setConditionEndpoint(setCondition(testConditionNames[1], jsonSchemas[1]))
        await permitEndpoint(permit(testRoles[0], testConditionNames[0]))
        await restrictEndpoint(restrict(testRoles[0], testConditionNames[1]))
        await permitEndpoint(permit(testRoles[1], testConditionNames[0]))

        await expectTableOK(RULES_TABLE_CFG, getTableRowContentForRules(pm.getRules()))
    })
})



describe("test reading", () => {
    beforeEach(async () => {
        const existingTables = await core.events.request(listTables()) as string[]
        for (const tableConfig of [ ROLES_TABLE_CFG, CONDITIONS_TABLE_CFG, 
                                    USER_ROLES_TABLE_CFG, RULES_TABLE_CFG]) {
            if (existingTables.includes(tableConfig.tableName)) {
                await core.events.request(deleteRow(
                    tableConfig.tableName, []))
            }
        }
    })

    test("read roles", async () => {
        const rolesToInsert = [
            { 
                [ROLES_TABLE_CFG.columns["role"].name]:     testRoles[0], 
                [ROLES_TABLE_CFG.columns["index"].name]:    0,
            }, {
                [ROLES_TABLE_CFG.columns["role"].name]:     testRoles[1], 
                [ROLES_TABLE_CFG.columns["index"].name]:    1,
            }
        ]

        await core.events.request(insert(ROLES_TABLE_CFG.tableName, rolesToInsert))
        await initDatabaseAccess(core.events)

        expect(await getRolesEndpoint(getRoles())).toEqual([testRoles[0], testRoles[1]])
    })

    test("read conditions", async () => {
        const conditionsToInsert = [
            { 
                [CONDITIONS_TABLE_CFG.columns["name"].name]:            testConditionNames[0], 
                [CONDITIONS_TABLE_CFG.columns["jsonSchema"].name]:      JSON.stringify(jsonSchemas[0]),
            }, {
                [CONDITIONS_TABLE_CFG.columns["name"].name]:            testConditionNames[1], 
                [CONDITIONS_TABLE_CFG.columns["jsonSchema"].name]:      JSON.stringify(jsonSchemas[1]),
            }
        ]

        await core.events.request(insert(CONDITIONS_TABLE_CFG.tableName, conditionsToInsert))
        await initDatabaseAccess(core.events)

        expect(await listConditionsEndpoint()).toEqual([testConditionNames[0], testConditionNames[1]])
        expect(await getConditionEndpoint(getCondition(testConditionNames[0]))).toEqual(jsonSchemas[0])
        expect(await getConditionEndpoint(getCondition(testConditionNames[1]))).toEqual(jsonSchemas[1])
    })

    test("read user roles", async () => {
        const rolesToInsert = [
            { 
                [ROLES_TABLE_CFG.columns["role"].name]:     testRoles[0], 
                [ROLES_TABLE_CFG.columns["index"].name]:    0,
            }, {
                [ROLES_TABLE_CFG.columns["role"].name]:     testRoles[1], 
                [ROLES_TABLE_CFG.columns["index"].name]:    1,
            }
        ]
        const userRolesToInsert = [
            { 
                [USER_ROLES_TABLE_CFG.columns["user"].name]:    testUsernames[0], 
                [USER_ROLES_TABLE_CFG.columns["role"].name]:    testRoles[0],
            }, {
                [USER_ROLES_TABLE_CFG.columns["user"].name]:    testUsernames[0], 
                [USER_ROLES_TABLE_CFG.columns["role"].name]:    testRoles[1],
            }, { 
                [USER_ROLES_TABLE_CFG.columns["user"].name]:    testUsernames[1], 
                [USER_ROLES_TABLE_CFG.columns["role"].name]:    testRoles[0],
            }, {
                [USER_ROLES_TABLE_CFG.columns["user"].name]:    testUsernames[2], 
                [USER_ROLES_TABLE_CFG.columns["role"].name]:    testRoles[1],
            }, 
        ]

        await core.events.request(insert(ROLES_TABLE_CFG.tableName, rolesToInsert))
        await core.events.request(insert(USER_ROLES_TABLE_CFG.tableName, userRolesToInsert))
        await initDatabaseAccess(core.events)

        expect(await listRolesOfUserEndpoint(listRolesOfUser(testUsernames[0]))).toEqual([testRoles[0], testRoles[1]])
        expect(await listRolesOfUserEndpoint(listRolesOfUser(testUsernames[1]))).toEqual([testRoles[0]])
        expect(await listRolesOfUserEndpoint(listRolesOfUser(testUsernames[2]))).toEqual([testRoles[1]])
    })

    test("read rules", async () => {
        const rolesToInsert = [
            { 
                [ROLES_TABLE_CFG.columns["role"].name]:     testRoles[0], 
                [ROLES_TABLE_CFG.columns["index"].name]:    0,
            }, {
                [ROLES_TABLE_CFG.columns["role"].name]:     testRoles[1], 
                [ROLES_TABLE_CFG.columns["index"].name]:    1,
            }
        ]
        const conditionsToInsert = [
            { 
                [CONDITIONS_TABLE_CFG.columns["name"].name]:            testConditionNames[0], 
                [CONDITIONS_TABLE_CFG.columns["jsonSchema"].name]:      JSON.stringify(jsonSchemas[0]),
            }, {
                [CONDITIONS_TABLE_CFG.columns["name"].name]:            testConditionNames[1], 
                [CONDITIONS_TABLE_CFG.columns["jsonSchema"].name]:      JSON.stringify(jsonSchemas[1]),
            }
        ]
        const rulesToInsert = [
            { 
                [RULES_TABLE_CFG.columns["role"].name]:             testRoles[0], 
                [RULES_TABLE_CFG.columns["condition"].name]:        testConditionNames[0],
                [RULES_TABLE_CFG.columns["permissionType"].name]:   PermissionType.permitted,
            }, { 
                [RULES_TABLE_CFG.columns["role"].name]:             testRoles[0], 
                [RULES_TABLE_CFG.columns["condition"].name]:        testConditionNames[1],
                [RULES_TABLE_CFG.columns["permissionType"].name]:   PermissionType.restricted,
            }, { 
                [RULES_TABLE_CFG.columns["role"].name]:             testRoles[1], 
                [RULES_TABLE_CFG.columns["condition"].name]:        testConditionNames[0],
                [RULES_TABLE_CFG.columns["permissionType"].name]:   PermissionType.permitted,
            },
        ]
        

        await core.events.request(insert(ROLES_TABLE_CFG.tableName, rolesToInsert))
        await core.events.request(insert(CONDITIONS_TABLE_CFG.tableName, conditionsToInsert))
        await core.events.request(insert(RULES_TABLE_CFG.tableName, rulesToInsert))
        await initDatabaseAccess(core.events)

        expect(await listPermissionsEndpoint(listPermissions(testRoles[0]))).toEqual([testConditionNames[0]])
        expect(await listRestrictionsEndpoint(listRestrictions(testRoles[0]))).toEqual([testConditionNames[1]])
        expect(await listPermissionsEndpoint(listPermissions(testRoles[1]))).toEqual([testConditionNames[0]])
        expect(await listRestrictionsEndpoint(listRestrictions(testRoles[1]))).toEqual([])
    })
})